from re import compile
from typing import Match


def is_script_date(date: str) -> Match:
    date_re = compile((r'^(3[01]|[12][0-9]|[1-9]) +'
                       r'([jJ]anvier|[fF]évrier|[mM]ars|[aA]vril|[mM]ai|[jJ]uin|'
                       r'[jJ]uillet|[aA]oût|[sS]eptembre|[oO]ctobre|[nN]ovembre|[dD]écembre|[789X]\^bre) +'
                       r'(1[0-9][0-9][0-9]|[5-9][0-9][0-9])$'))
    return date_re.fullmatch(date)
