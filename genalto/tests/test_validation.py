from ..validation import is_script_date


def test_date_val():
    assert is_script_date('1 janvier 1902')
    assert is_script_date('5 Décembre 1856')
    assert is_script_date('22 X^bre 804')
